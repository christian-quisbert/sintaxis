# README #

-------------------------------------------------------------------------------------------
"IMPORTANTE"
SIEMPRE hacer 'pull' antes de 'push' en caso de que alguien haya subido cambios al servidor.
-------------------------------------------------------------------------------------------

Crear repositorio local en Linux:
*********************************
$ mkdir ~/repo
$ cd ~/repo
$ git clone https://christian-quisbert@bitbucket.org/christian-quisbert/sintaxis.git
$ git init
$ ls -l # Listar el contenido del repo

Agregar cambios al repositorio Local (MiPc):
*********************************************
$ git diff	# Muestra las diferencias del archvivo en consola
$ git add .	# Agrego las modificaciones al repo local
$ git status	# Muestra listado de cambios en consola
$ git commit	# Hace commit en el repo local (Pedirá agregar un nombre al cambio)
$ git log	# Muestra los commits hechos.

Subir los cambios al repositorio GLobal (Bitbucket):
****************************************************
# git push [remote-name] [branch-name]
$ git push origin master

Traer los cambio al repositorio Local (MiPc):
*********************************************
$ git pull	#Traigo lo último, "todo" lo que haya en el repositorio Global.