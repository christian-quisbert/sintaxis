/**
		\file    main.c
		\brief   Contiene el main principal del "Reconecedor de cadenas" de un lenguaje x.
		\author  Chistian Leonel Quisbert (christianquisbert@gmail.com)
		\date    2016.05.18
		\version Versión 1.0.1
*/
#include <stdio.h>
#include <ctype.h>
#include "funciones.h"

int main()
{
	int estado=q0;
	char c;
	int a = 0;
	int TT[5][6] = {
	{q1 , q1 , q2 , q2 , q2 , q4},
	{q1 , q1 , q1 , q1 , q2 , q3},
	{q2 , q2 , q2 , q2 , q2 , q4},
	{q3 , q3 , q3 , q3 , q3 , q3},
	{q4 , q4 , q4 , q4 , q4 , q4}};
	
	printf("Ingrese una palabra: ");
	while ((estado != q3) && (estado != q4))
	{
		c = getc(stdin);
		if(!a) printState('-',q0); a++;	//Para imprimir estado inicial.
		estado = TT[estado][posColumna(c)];
		printState(c,estado);
	}
	if(estado==q3){printf("\n\nPalabra Válida.\n");}
	if(estado==q4){printf("\n\nPalabra Rechazada.\n");}
	
	return 0;
}
