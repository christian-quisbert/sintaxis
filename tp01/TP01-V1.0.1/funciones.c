/**
		\file    funciones.c
		\brief   Contiene las funciones para main.c
		\author  Chistian Leonel Quisbert (christianquisbert@gmail.com)
		\date    2016.05.18
		\version Versión 1.0.1
*/
#include <stdio.h>
#include "funciones.h"
/**
		\fn     posColumna
		\brief  Devuelve la COLUMNA correspondiente al caracter ingresado.
		\author Chistian Leonel Quisbert (christianquisbert@gmail.com)
		\date   2016.05.18
		\param  char c
		\return int col
*/
int posColumna (char c)
{	
	/*Le asigno un caracter para comparar..*/
	if(islower(c))	{c = 'a';}
	if(isdigit(c))	{c = '1';}
	
	switch(c)
	{
		case 'a':
			return lwrs;
			break;
		case '1':
			return dgts;
			break;
		case '_':
			return gion;
			break;
		case '%':
			return porc;
			break;
		case '\n':
			return rtrn;
			break;
		default:
			return othr;
			break;
	}
}
void printState(char c, int a)
{
	printf("\n => ");
	switch(a)
	{
		case q0:
			printf("(q0)");
			break;
		case q1:
			printf("%c (q1)",c);
			break;
		case q2:
			printf("%c (q2)",c);
			break;
		case q3:
			printf("(q3)");
			break;
		case q4:
			printf("(q4)");
			break;
		default:
			break;
	}
}

