/*Me indican la Columna*/
#define	lwrs	0	/* ['a'..'z'] */
#define	gion	1	/* '-' */
#define	dgts	2	/* ['0'..'9']*/
#define	porc	3	/* '%' */
#define	rtrn	5	/* '\n' */
#define	othr	4	/* others */

/*Me indican la Fila*/
#define	q0	0
#define	q1	1
#define	q2	2
#define	q3	3
#define	q4	4

/*
+-----------------------------------------------------------+
|     |'a'-'z' |  '_'   | '0'-'9'|  '%'   |  otros |  '\n'  |
+-----------------------------------------------------------+
| q0- |   q1   |   q1   |   q2   |   q2   |   q2   |   q4   |
+-----------------------------------------------------------+
| q1  |   q1   |   q1   |   q1   |   q1   |   q2   |   q3   |
+-----------------------------------------------------------+
| q2  |   q2   |   q2   |   q2   |   q2   |   q2   |   q4   |
+-----------------------------------------------------------+
| q3+ | VALIDA | VALIDA | VALIDA | VALIDA | VALIDA | VALIDA |
+-----------------------------------------------------------+
| Q4+ |RECHAZA |RECHAZA |RECHAZA |RECHAZA |RECHAZA |RECHAZA |
+-----------------------------------------------------------+
*/

int posColumna (char);
void printState (char,int);
