/**
		\file    main.c
		\brief   Contiene el main principal del "Reconecedor de cadenas" de un lenguaje x.
		\author  Chistian Leonel Quisbert (christianquisbert@gmail.com)
		\date    2016.05.15
		\version Versión 1.0.0
*/
#include <stdio.h>
#include <ctype.h>
#include "funciones.h"

int main()
{
	int estado=q0;
	char c;
	int TT[5][6] = {
	/*----*//*let, _ , n° , %  , x  , \n */
	/*q0=0*/{q1 , q1 , q2 , q2 , q2 , q4},
	/*q1=1*/{q1 , q1 , q1 , q1 , q2 , q3},
	/*q2=2*/{q2 , q2 , q2 , q2 , q2 , q4},
	/*q3=3*/{q3 , q3 , q3 , q3 , q3 , q3},
	/*q4=4*/{q4 , q4 , q4 , q4 , q4 , q4}};
	
	while ((estado != q3) && (estado != q4))
	{
		c = getc(stdin);
		estado = TT[estado][posColumna(c)];
	}
	if(estado==q3){printf("\nPalabra Válida.");}
	if(estado==q4){printf("\nPalabra Rechazada.");}
	
	return 0;
}
